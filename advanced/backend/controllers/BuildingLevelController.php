<?php

namespace backend\controllers;

use Yii;
use backend\models\BuildingLevel;
use backend\models\BuildingLevelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BuildingLevelController implements the CRUD actions for BuildingLevel model.
 */
class BuildingLevelController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BuildingLevel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BuildingLevelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BuildingLevel model.
     * @param integer $building_id
     * @param integer $building_level
     * @return mixed
     */
    public function actionView($building_id, $building_level)
    {
        return $this->render('view', [
            'model' => $this->findModel($building_id, $building_level),
        ]);
    }

    /**
     * Creates a new BuildingLevel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BuildingLevel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'building_id' => $model->building_id, 'building_level' => $model->building_level]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BuildingLevel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $building_id
     * @param integer $building_level
     * @return mixed
     */
    public function actionUpdate($building_id, $building_level)
    {
        $model = $this->findModel($building_id, $building_level);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'building_id' => $model->building_id, 'building_level' => $model->building_level]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BuildingLevel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $building_id
     * @param integer $building_level
     * @return mixed
     */
    public function actionDelete($building_id, $building_level)
    {
        $this->findModel($building_id, $building_level)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BuildingLevel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $building_id
     * @param integer $building_level
     * @return BuildingLevel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($building_id, $building_level)
    {
        if (($model = BuildingLevel::findOne(['building_id' => $building_id, 'building_level' => $building_level])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
