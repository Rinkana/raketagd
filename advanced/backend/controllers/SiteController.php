<?php
namespace backend\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\db\Command;
use yii\db\Connection;
use backend\models\Dbhelper;
use backend\models\Csvupload;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','tables','columns','rows','getrows','upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionTables(){

        $sql = "select table_name from information_schema.tables where table_catalog='dev_tests'
  and table_schema not in ('pg_catalog','information_schema')
  and table_name not LIKE 'user'
  and table_name not LIKE 'migration'
  ORDER BY table_name ASC";
        $connection = Yii::$app->db;
        $tables = $connection->createCommand($sql)->queryAll();
        $dp = new ArrayDataProvider([
            'allModels' => $tables,
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
        return $this->render('tables', [
            'dp' => $dp,
        ]);
    }

    public function actionColumns($name='building_level'){
        $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '$name' ";
        $connection = Yii::$app->db;
        $columns = $connection->createCommand($sql)->queryAll();
        $table_info = $connection->getTableSchema($name);
        $pk_arr = $table_info->primaryKey;
        foreach($pk_arr as $key_item){
            foreach ($columns as $count_key_item=>$col_item){
                if (in_array($key_item,$col_item)){
                    unset($columns[$count_key_item]);
                }
            }
        }
        $dp = new ArrayDataProvider([
            'allModels' => $columns,
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
        return $this->render('columns', [
            'dp' => $dp,
            'table_name'=>$name,
        ]);
    }

    public function actionRows($name = 'building_level'){
        $request = Yii::$app->request;
        $columns_arr = $request->get('columns');
        $connection = Yii::$app->db;
        $table_info = $connection->getTableSchema($name);
        $pk_arr = $table_info->primaryKey;
        foreach($pk_arr as $pk_col)
            array_unshift($columns_arr,$pk_col);
        $columns = implode(',',$columns_arr);
        $scheme = $connection->getSchema()->defaultSchema;
        $sql = "SELECT $columns FROM $scheme.$name";
        $rows = $connection->createCommand($sql)->queryAll();
        $dp = new ArrayDataProvider([
            'allModels' => $rows,
            'pagination' => [
                'pageSize' => 10000,
            ],
        ]);
        return $this->render('rows', [
            'dp' => $dp,
            'table_name'=>$name,
            'columns'=>$columns_arr,
            'npk'=>$pk_arr,//primary key returned from function
        ]);
    }

    public function actionGetrows($name = 'building_level'){
        $request = Yii::$app->request;
        $columns_arr = $request->get('columns');
        $connection = Yii::$app->db;
        $table_info = $connection->getTableSchema($name);
        $pk_arr = $table_info->primaryKey;
        foreach($pk_arr as $pk_col)
            array_unshift($columns_arr,$pk_col);
        $row_ids = $request->get('rows');
        $columns = implode(',',$columns_arr);


        $scheme = $connection->getSchema()->defaultSchema;
        $pk_str='';
        foreach($row_ids as $row){
            $r_arr = explode(',',$row);
            $buf = '1=1';
            foreach($r_arr as $key=>$item){
                $buf.= " AND ".$pk_arr[$key]."=".$item;
            }
            $pk_str .= " (".$buf.") OR";
        }
        $pk_str = substr($pk_str, 0, -3);
        $sql = "SELECT $columns FROM $scheme.$name WHERE $pk_str;";
        $info = $connection->createCommand($sql)->queryAll();
        $i_help = new Dbhelper();
        $filename = $name."__".date('d-m-y_H:i:s');
        $answer_array = array();
        foreach ($info as $i_key=>$i_val){
                foreach($i_val as $json_key=>$json_item){
                    if (!in_array($json_key,$pk_arr)){
                        $json = json_decode($i_val[$json_key]);
                        if (gettype($json) == 'object'){
                            foreach ($json as $json_items_key=>$json_items_value){
                                if (gettype($json_items_value) == 'object'){
                                    $buf_arr = array($json_key."_".$json_items_key);
                                    array_push($answer_array,$buf_arr);
                                    foreach($json_items_value as $json_item_key=>$json_item_value){
                                        $buf_arr = [$json_item_key,$json_item_value];
                                        array_push($answer_array,$buf_arr);
                                    }
                                }
                            }
                        }
                    }
                }
        }
        //$answer_array = array_unique($answer_array);
        print_r($answer_array);
        //var_dump($info);


        die();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}.csv");
        header("Pragma: no-cache");
        header("Expires: 0");


        $i_help->outputCSV($info);

    }
//insert query: INSERT INTO common.building_level VALUES(5007,2,'{"item": {"1": 10,"3": 100},"unit": {"15": 5,"25": 10}}','{"iprops": [0,9,7,5],"price": {"item": {"1": 10,"3": 100},"unit": {"15": 5,"25": 10}},"mine": {"3": 1},"unit": {"20": 2},"tech": {"100": 3},"chest": {"5": 1}}','{"iprops": [0,5,9,8,7],"price": {"item": {"1": 10,"3": 100},"unit": {"15": 5,"25": 10}},"building_level": {"1": {"1": 3,"2": 5}},"tech": {"7": 2},"quest": {"33": 5}}');
  public function actionUpload(){
      $model = new Csvupload();

      if (Yii::$app->request->isPost) {
          $model->file = UploadedFile::getInstance($model, 'file');
          if ($model->file && $model->validate()) {
              if ($model->file->extension != 'csv'){
                  \Yii::$app->session->setFlash('upload_error','wrong type of file');
              }else{
                  $fp = Yii::$app->basePath.'/web/uploads/' . $model->file->baseName . '.' . $model->file->extension;
                  $model->file->saveAs($fp);
                  $db_name = explode('__',$model->file->baseName);
                  $db_name = $db_name[0];
                  $connection = Yii::$app->db;
                  $scheme = $connection->getSchema()->defaultSchema;
                  $table_info = $connection->getTableSchema($db_name);
                  $pk_arr = $table_info->primaryKey;
                  $i_helper = new Dbhelper();
                  $csv = $i_helper->getCSV($fp);
                  $arr_headers = $csv[0];
                  $in_pk_arr = true;
                  foreach ($pk_arr as $pk_item){
                      if (!in_array($pk_item,$arr_headers)){
                          $in_pk_arr =false;
                      }
                  }
                  if ($in_pk_arr){
                      array_shift($csv);
                      foreach($csv as $csv_line_key => $csv_line_item){
                          $sql_upd = "UPDATE ".$scheme.".".$db_name." SET ";
                          $sql_item_upd = '';
                          $sql_item_pk = 'WHERE 1=1';
                          foreach ($arr_headers as $arr_header_key=>$arr_header_item){
                              if (in_array($arr_header_item,$pk_arr)){
                                  $sql_item_pk.= " AND ".$arr_header_item.'='.$csv_line_item[$arr_header_key];
                              }else{
                                  $item_to_insert = ($csv_line_item[$arr_header_key][0] == "{")? "'".str_replace('\'','\'\'',$csv_line_item[$arr_header_key])."'":$csv_line_item[$arr_header_key];
                                  $sql_item_upd.= " \"".$arr_header_item.'"='.$item_to_insert.',';
                              }
                          }
                          $sql_item_upd = substr($sql_item_upd, 0, -1);
                          $sql_item_upd.=" ";
                          $sql_upd .= $sql_item_upd.$sql_item_pk.";";
                          $info = $connection->createCommand($sql_upd)->queryOne();
                      }
                      @unlink($fp);//TODO add some checks for file
                      \Yii::$app->session->setFlash('upload_ok','success');
                  }else{
                      \Yii::$app->session->setFlash('upload_error','no pk fields');
                      @unlink($fp);//TODO add some checks for file
                  }
              }
          }
      }

      return $this->render('csvupload', ['model' => $model]);
  }
}
