<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BuildingLevel;

/**
 * BuildingLevelSearch represents the model behind the search form about `backend\models\BuildingLevel`.
 */
class BuildingLevelSearch extends BuildingLevel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['building_id', 'building_level'], 'integer'],
            [['price', 'reward', 'condition'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuildingLevel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'building_id' => $this->building_id,
            'building_level' => $this->building_level,
        ]);

        $query->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'reward', $this->reward])
            ->andFilterWhere(['like', 'condition', $this->condition]);

        return $dataProvider;
    }
}
