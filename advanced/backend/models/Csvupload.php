<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "building_level".
 *
 */
class Csvupload extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file',
                'skipOnEmpty' => false,
                //'extensions' => 'csv',
                //'mimeTypes' => 'text/plain'
            ],

        ];
    }
}
