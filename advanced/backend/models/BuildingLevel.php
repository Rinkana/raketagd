<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "building_level".
 *
 * @property integer $building_id
 * @property integer $building_level
 * @property string $price
 * @property string $reward
 * @property string $condition
 */
class BuildingLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'building_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['building_id', 'building_level'], 'required'],
            [['building_id', 'building_level'], 'integer'],
            [['price', 'reward', 'condition'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'building_id' => 'Building ID',
            'building_level' => 'Building Level',
            'price' => 'Price',
            'reward' => 'Reward',
            'condition' => 'Condition',
        ];
    }
}
