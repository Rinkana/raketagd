<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for work with db.

 */
class Dbhelper {

    public static function buildQuery($template, $values, $values_2 = array()) {
        foreach ($values as $key => $value) {
            if (gettype($value) == 'string') $values[$key] = "'" . pg_escape_string($value) . "'";
        }

        $values_row = 'ROW(' . implode(',', $values) . ')';
        $values_row_2 = '';

        if (isset($values_2) && !empty($values_2)) {
            foreach ($values_2 as $key => $value) {
                if (gettype($value) == 'string') $values_2[$key] = "'" . pg_escape_string($value) . "'";
            }

            $values_2_row = 'ROW(' . implode(',', $values_2) . ')';
        }

        if (!empty($values_2_row)) $values_row = $values_row . ', ' . $values_2_row;

        $sql = preg_replace('/\(.+\)/', '(' . $values_row . ')', $template);

        debug_log('buildQuery ' . $sql);

        return $sql;
    }

    public static function arrayToCsv( array &$fields, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ( $fields as $field ) {
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }

            // Enclose fields containing $delimiter, $enclosure or whitespace
            if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
                $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
            }
            else {
                $output[] = $field;
            }
        }

        return implode( $delimiter, $output );
    }

    public function outputCSV($data) {
        $outputBuffer = fopen("php://output", 'w');
        foreach($data as $val) {
            fputcsv($outputBuffer, $val,';');
        }
        fclose($outputBuffer);
    }

    public function getCSV($filepath){
        $handle = fopen($filepath, "r"); //Открываем csv для чтения
        $array_line_full = array(); //Массив будет хранить данные из csv
        //Проходим весь csv-файл, и читаем построчно. 3-ий параметр разделитель поля
        while (($line = fgetcsv($handle, 0, ";")) !== FALSE) {
            $array_line_full[] = $line; //Записываем строчки в массив
        }
        fclose($handle); //Закрываем файл
        return $array_line_full;
    }

    public static function transponirovat($data, $row_indexes, $col_index, $cell_index) {
        $return = array();
        $return['data'] = $data;

        $finalColumns = array_fill(0, count($row_indexes), 0);
        $tempColumns = array();
        $tempRows = array();

        foreach ($data as $dataKey => $dataValue) {
            array_push($tempColumns, $dataValue[$col_index]);

            $arIndex = array();

            foreach ($row_indexes as $indexValue) {
                array_push($arIndex, $dataValue[$indexValue]);
            }

            $index = implode('~', $arIndex);

            if (!isset($tempRows[$index])) $tempRows[$index] = array();

            array_push($tempRows[$index], $dataValue);
        }

        $tempColumns = array_unique($tempColumns);
        sort($tempColumns);

        $finalColumns = array_merge($finalColumns, $tempColumns);

        ksort($tempRows);
        $finalRows = array();
        //$return['tempRows'] = $tempRows;

        foreach ($tempRows as $tempRowKey => $tempRowValues) {
            $finalRow = explode('~', $tempRowKey);

            foreach ($tempColumns as $tempColumnKey => $tempColumnValue) {
                $cellValue = 0;

                foreach ($tempRowValues as $tempRowValueKey => $tempRowValueValue) {
                    if ($tempRowValueValue[$col_index] == $tempColumnValue) $cellValue = $tempRowValueValue[$cell_index];
                }

                array_push($finalRow, $cellValue);
            }

            array_push($finalRows, $finalRow);
        }

        $return['columns'] = $finalColumns;
        $return['rows'] = $finalRows;

        return $return;
    }

    public static function transponirovatEC($data, $row_indexes, $col_index, $cell_index, $step = 1, $start = 0, $end = 0) {
        $return = array();
        $return['data'] = $data;

        $finalColumns = array_fill(0, count($row_indexes), 0);
        $tempColumns = array();
        $tempRows = array();
        $allColumns = array();
        for ($i = (int)$start; $i < (int)$end; $i+=$step){
            array_push($allColumns,$i);
        }
        //array_push($allColumns, (int)$end);
        foreach ($allColumns as $key => $value) {
            $value = $value+'';
            array_push($tempColumns, $value);
        }

        foreach ($data as $dataKey => $dataValue) {
            array_push($tempColumns, $dataValue[$col_index]);

            $arIndex = array();

            foreach ($row_indexes as $indexValue) {
                array_push($arIndex, $dataValue[$indexValue]);
            }

            $index = implode('~', $arIndex);

            if (!isset($tempRows[$index])) $tempRows[$index] = array();

            array_push($tempRows[$index], $dataValue);
        }

        $tempColumns = array_unique($tempColumns);
        sort($tempColumns);

        $finalColumns = array_merge($finalColumns, $tempColumns);

        ksort($tempRows);
        $finalRows = array();

        foreach ($tempRows as $tempRowKey => $tempRowValues) {
            $finalRow = explode('~', $tempRowKey);

            foreach ($tempColumns as $tempColumnKey => $tempColumnValue) {
                $cellValue = 0;

                foreach ($tempRowValues as $tempRowValueKey => $tempRowValueValue) {
                    if ($tempRowValueValue[$col_index] == $tempColumnValue) $cellValue = $tempRowValueValue[$cell_index];
                }

                array_push($finalRow, $cellValue);
            }

            array_push($finalRows, $finalRow);
        }

        $return['columns'] = $finalColumns;
        $return['rows'] = $finalRows;

        return $return;
    }

}
