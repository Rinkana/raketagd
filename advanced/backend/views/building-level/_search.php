<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BuildingLevelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="building-level-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'building_id') ?>

    <?= $form->field($model, 'building_level') ?>

    <?= $form->field($model, 'price') ?>

    <?= $form->field($model, 'reward') ?>

    <?= $form->field($model, 'condition') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
