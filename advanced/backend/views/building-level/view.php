<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BuildingLevel */

$this->title = $model->building_id;
$this->params['breadcrumbs'][] = ['label' => 'Building Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="building-level-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'building_id' => $model->building_id, 'building_level' => $model->building_level], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'building_id' => $model->building_id, 'building_level' => $model->building_level], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'building_id',
            'building_level',
            'price:ntext',
            'reward:ntext',
            'condition:ntext',
        ],
    ]) ?>

</div>
