<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BuildingLevel */

$this->title = 'Update Building Level: ' . ' ' . $model->building_id;
$this->params['breadcrumbs'][] = ['label' => 'Building Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->building_id, 'url' => ['view', 'building_id' => $model->building_id, 'building_level' => $model->building_level]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="building-level-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
