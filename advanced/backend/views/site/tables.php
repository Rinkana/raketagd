<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'View tables';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        echo  GridView::widget([
            'dataProvider' => $dp,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'table_name',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'Actions',
                    'template' => '{list}',
                    'buttons' => [

                        //view button
                        'list' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-list"></span>', $url, [
                                    'title' => '',
                                    'class'=>'',
                                ]);
                            },
                    ],

                    'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'list') {
                                $url ='/site/columns?name='.$model['table_name'];
                                return $url;
                            }
                        }

                ],
            ],
        ]);
?>

</div>