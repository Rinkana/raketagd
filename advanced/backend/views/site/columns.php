<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Columns: '.$table_name;
$this->params['breadcrumbs'][] = ['label' => 'Tables', 'url' => ['site/tables']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    //var keys = $('#grid').yiiGridView('getSelectedRows');
    echo  GridView::widget([
        'dataProvider' => $dp,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) {
                        return ['value' => $model['column_name']];
                    }

            ],
            'column_name',

        ],
        'options'=>['class'=>'js_get_columns_grid'],
    ]);
    ?>
    <p>
        <?= Html::a('Choose columns', ['#'], ['class' => 'btn btn-success js_get_selected_columns']) ?>
    </p>

</div>