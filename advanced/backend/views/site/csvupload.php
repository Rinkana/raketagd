<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Alert;

$this->title = 'Upload file';
?>
<?php
if(Yii::$app->session->hasFlash('upload_error')){
    echo Alert::widget([
        'options' => [
            'class' => 'alert-danger',
        ],
        'body' => Yii::$app->session->getFlash('upload_error'),
    ]);
} ?>

<?php
if(Yii::$app->session->hasFlash('upload_ok')){
    echo Alert::widget([
        'options' => [
            'class' => 'alert-success',
        ],
        'body' => Yii::$app->session->getFlash('upload_ok'),
    ]);
} ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <h1><?= Html::encode($this->title) ?></h1>

<?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Upload', ['class' =>  'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end() ?>