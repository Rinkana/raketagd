<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Rows: ';
$this->params['breadcrumbs'][] = ['label' => 'Tables', 'url' => ['site/tables']];
$this->params['breadcrumbs'][] = ['label' => 'Columns', 'url' => ['site/columns',['name'=>$table_name]]];
$this->params['breadcrumbs'][] = $this->title;
?>


<div>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    array_unshift($columns,
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function($model, $key, $index, $column) use ($npk) {
                    $keys = array_keys($model);
                    $str='';
                    foreach($npk as $n_key){
                        $str.=$model[$n_key].',';
                    }
                    /*for ($i=0; $i < $npk; $i++){
                        $str.=$model[$keys[$i]].',';
                    }*/
                    $str = substr($str, 0, -1);
                    return ['value' => $str];
                }

        ]
    );

    echo  GridView::widget([
        'dataProvider' => $dp,
        'columns' => $columns,
        'options'=>['class'=>'js_get_rows_grid'],
    ]);
    ?>
    <p>
        <?= Html::a('Get csv', ['#'], ['class' => 'btn btn-success js_get_selected_rows']) ?>
    </p>

</div>